#! /bin/bash
#
# Usage:
#   ./install_all.sh

echo "============================================================"
echo "start uninstall cnpy"
echo "============================================================"
cd cnpy/build
cat install_manifest.txt | sudo xargs rm
cd ../..
sudo rm -r cnpy
echo "============================================================"
echo "finish uninstall cnpy"
echo "============================================================"

