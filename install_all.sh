#! /bin/bash
#
# Usage:
#   ./install_all.sh

echo "============================================================"
echo "start install cnpy"
echo "============================================================"

git clone https://github.com/rogersce/cnpy.git
echo $CMD
cd cnpy
mkdir build 
cd build
cmake .. 
make -j2
sudo make install
cd ../..
make
echo "============================================================"
echo "install cnpy finished"
echo "============================================================"
