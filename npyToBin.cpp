#include <cnpy.h>

#include <fstream>
#include <iomanip>
#include <vector>
#define DIM 4096

using namespace std;

vector<float> readnpy_input1D(const string& npy_name, int read_num) {
    cnpy::NpyArray arr = cnpy::npy_load(npy_name);
    vector<float> read_vec(read_num);
    float* loaded_data = arr.data<float>();

    for (int i = 0; i < read_num; ++i) {
        read_vec[i] = loaded_data[i];
    }
    return read_vec;
}

int main(int argc, const char* argv[]) {
    // for(int i = 0; i<32; ++i){
    string location0 = argv[1];
    string location1 = argv[2];
    std::cout << location0 << "\n";
    std::cout << location1 << "\n";
    int num_numbers = std::stoi(argv[3]);
    std::cout << num_numbers << "\n";
    // string location0 = "/mnt/raid5/hanzhong/llama_7B_data/transformer_data/transfomer_ouptut_layer" +
    // std::to_string(1) + ".npy";
    vector<float> read_x = readnpy_input1D(location0, num_numbers);
    for (int i = 0; i < read_x.size(); ++i) {
        if (i == 0) {
            printf("%f ", read_x[i]);
        }
    }
    //}}

    std::ofstream ofs2(location1, std::ios::binary);
    ofs2.write(reinterpret_cast<char*>(&read_x[0]), read_x.size() * sizeof(_Float32));
    ofs2.close();

    return 0;
}
